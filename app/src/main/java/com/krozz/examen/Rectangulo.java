package com.krozz.examen;

import java.io.Serializable;

public class Rectangulo implements Serializable {

    private int altura;
    private int base;


    public Rectangulo(int base,int altura ){
        this.setbase(base);
        this.setaltura(altura);
    }

    public Rectangulo(){

    }

    public int getaltura() {
        return altura;
    }

    public void setaltura(int altura) {
        this.altura = altura;
    }

    public int getbase() {
        return base;
    }

    public void setbase(int base) {
        this.base = base;
    }

    public float calcularArea(){


        int area = base * altura;

        return  area;
    }

    public  float calcularPerimetro(){

        int perimetro = (base+altura) * 2;

        return  perimetro;
    }
}
